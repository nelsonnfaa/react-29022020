import React, { useState } from 'react';

export default function SingleFile() {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');

  function handleFormSubmit(e) {
    e.preventDefault();
    const { elements } = e.target;
    const formData = new FormData();

    formData.append('name', name);
    formData.append('age', age);

    for (const inputField of elements) {
      if (inputField.type === 'file') {
        for (const fileIndex in inputField.files) {
          formData.append(inputField.name, inputField.files[fileIndex]);
        }
      }
    }

    fetch('http://localhost:3004/file/upload-multiple', {
      method: 'POST',
      body: formData
    })
      .then(res => {
        if (!res.ok) {
          throw new Error('Error uploading file');
        }

        return res.json();
      })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  return (
    <form onSubmit={handleFormSubmit}>
      <input
        type="text"
        value={name}
        onChange={e => setName(e.target.value)}
        placeholder="The Name"
      />
      <input
        type="number"
        value={age}
        onChange={e => setAge(e.target.value)}
        placeholder="The Age"
      />
      <input
        type="file"
        name="pictures"
        accept="image/png, image/jpeg"
        multiple
      />
      <button type="submit">Submit form</button>
    </form>
  );
}
